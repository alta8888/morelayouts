# MoreLayouts for Thunderbird

MoreLayouts adds additional Wide Thread View and Stacked View to Thunderbird along with Reversed views. Plus configurable Fullscreen F11 and Message Pane F8 toggles. The options are found in the View->Layout or AppMenu Options|Preferences->Layout menus.